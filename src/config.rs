use dirs::data_local_dir;
use serde::{Deserialize, Serialize};
use std::fs::{create_dir_all, File};
use std::io::prelude::*;
use std::io::Error;
use std::mem::replace;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub gpgid: String,
}

pub fn create_config() -> Config {
    Config {
        gpgid: String::from(""),
    }
}

impl Config {
    pub fn load(&mut self) -> Result<(), Error> {
        let mut buffer = String::from("");
        let mut file = match File::open(
            data_local_dir()
                .unwrap()
                .into_os_string()
                .into_string()
                .unwrap()
                + "/.password/.config.json",
        ) {
            Ok(t) => t,
            Err(e) => return Err(e),
        };
        file.read_to_string(&mut buffer).unwrap();
        let desirialized: Config = serde_json::from_str(&buffer).unwrap();
        let _old = replace(self, desirialized);
        Ok(())
    }
    pub fn save(&mut self) {
        let data = serde_json::to_string(&self).unwrap();
        let file = File::with_options().write(true).open(
            data_local_dir()
                .unwrap()
                .into_os_string()
                .into_string()
                .unwrap()
                + "/.password/.config.json",
        );
        let mut file = match file {
            Err(_e) => {
                create_dir_all(
                    data_local_dir()
                        .unwrap()
                        .into_os_string()
                        .into_string()
                        .unwrap()
                        + "/.password",
                )
                .unwrap();
                File::create(
                    data_local_dir()
                        .unwrap()
                        .into_os_string()
                        .into_string()
                        .unwrap()
                        + "/.password/.config.json",
                )
                .unwrap();
                File::with_options()
                    .write(true)
                    .open(
                        data_local_dir()
                            .unwrap()
                            .into_os_string()
                            .into_string()
                            .unwrap()
                            + "/.password/.config.json",
                    )
                    .unwrap()
            }
            Ok(t) => t,
        };
        file.write_all(data.as_bytes()).unwrap();
    }
}
