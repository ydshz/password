use gpgme::{Context, Protocol};

pub fn encrypt(text: &String, gpgid: &String) -> String {
    let mut encrypted_text = Vec::new();
    let proto = Protocol::OpenPgp;
    let mut context = Context::from_protocol(proto).unwrap();
    context.set_armor(true);
    let key = context.get_key(gpgid.to_string());
    context
        .encrypt(&key, text.to_string(), &mut encrypted_text)
        .unwrap();
    String::from_utf8(encrypted_text).unwrap()
}
pub fn decrypt(text: &String) -> String {
    let mut decrypted_text = Vec::new();
    let proto = Protocol::OpenPgp;
    let mut context = Context::from_protocol(proto).unwrap();
    context
        .decrypt(text.to_string(), &mut decrypted_text)
        .unwrap();
    String::from_utf8(decrypted_text).unwrap()
}
