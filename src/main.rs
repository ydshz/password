#![feature(with_options)]
extern crate clap;
use clap::{App, Arg, SubCommand};
use dirs::data_local_dir;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::io::Read;
mod config;
mod gpg;

fn main() {
    let app = App::new("Password-Manager")
        .version("0.1")
        .about("Stores your passwords")
        .subcommand(
            SubCommand::with_name("get")
                .about("Get a password from storage")
                .arg(
                    Arg::with_name("password")
                        .required(true)
                        .help("The password to get from the storage"),
                ),
        )
        .subcommand(
            SubCommand::with_name("create")
                .about("Save a new password in the storage")
                .arg(
                    Arg::with_name("name")
                        .required(true)
                        .help("The name of the password to store"),
                ),
        )
        .subcommand(SubCommand::with_name("list").about("List all passwords in the stash"));
    let matches = app.get_matches();
    let mut config = config::create_config();

    match config.load() {
        // Triggerd if theres no config
        Err(..) => {
            println!("Please enter your gpg id:");
            let mut gpgid = String::from("");
            let stdin = std::io::stdin();
            stdin.read_line(&mut gpgid).unwrap();
            if gpgid == String::from("\n") {
                println!("Please enter a valid gpg id");
                return;
            } else {
                config.gpgid = gpgid.replace("\n", "");
                config.save();
            }
        }
        _ => {}
    }
    match matches.subcommand() {
        ("get", Some(sub_match)) => get(sub_match.value_of("password").unwrap().to_string()),
        ("create", Some(sub_match)) => create(
            sub_match.value_of("name").unwrap().to_string(),
            config.gpgid,
        ),
        ("list", Some(_sub_match)) => list(),
        _ => list(),
    }
}

fn create(password_name: String, gpgid: String) {
    println!("Please enter the password to store:");
    let password1 = rpassword::read_password().unwrap();
    println!("Please retype your password:");
    if password1 == rpassword::read_password().unwrap() {
        File::create(
            data_local_dir()
                .unwrap()
                .into_os_string()
                .into_string()
                .unwrap()
                + "/.password/"
                + &password_name,
        )
        .unwrap();
        let mut password_file = File::with_options()
            .write(true)
            .open(
                data_local_dir()
                    .unwrap()
                    .into_os_string()
                    .into_string()
                    .unwrap()
                    + "/.password/"
                    + &password_name,
            )
            .unwrap();
        password_file
            .write_all(gpg::encrypt(&password1, &gpgid).as_bytes())
            .unwrap();
    } else {
        println!("Passwords are not matching.");
        return;
    }
}

fn get(password_name: String) {
    let mut file = File::open(
        data_local_dir()
            .unwrap()
            .into_os_string()
            .into_string()
            .unwrap()
            + "/.password/"
            + &password_name,
    )
    .unwrap();
    let mut encrypted_password = String::from("");
    file.read_to_string(&mut encrypted_password).unwrap();
    println!("{}", gpg::decrypt(&encrypted_password));
}

fn list() {
    let passwords = fs::read_dir(
        data_local_dir()
            .unwrap()
            .into_os_string()
            .into_string()
            .unwrap()
            + "/.password/",
    )
    .unwrap();
    println!("The following passwords were found in the storage:\n");
    for password in passwords {
        let password_name = password.unwrap().path().display().to_string();
        if password_name.split("/").last().unwrap() != ".config.json" {
            println!("- {}", password_name.split("/").last().unwrap());
        }
    }
}
